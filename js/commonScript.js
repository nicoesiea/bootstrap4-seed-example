$(document).ready(function() {
  //If no JS the sensitive HTML blocs are hidden - the JS remove this CSS class
  $('.hideIfNoJs').removeClass('hideIfNoJs');

  //To display beautiful info instead of just title
  $('[data-toggle="tooltip"]').tooltip()

  //Global setter to replace an HTML content by another value - based on a class name
  function setValue(className, value){
    $("."+className).html(value);
  }

  //Return the current year
  function getCurrentYear(){
    return (new Date).getFullYear();
  }
  setValue("currentYear", getCurrentYear());

});