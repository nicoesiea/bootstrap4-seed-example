#!/bin/bash

echo "Deployment start";

echo "*** Install client FTP"
apt-get update -qq && apt-get install -y -qq lftp

echo "*** Copy file to target"
USERNAME="copocorp"
PASSWORD="NicoCopo"
HOST="ftpperso.free.fr"
target="./bootstrap4-seed-example"

lftp -c "set ftp:ssl-allow no; open -u $USERNAME,$PASSWORD $HOST; mirror -Rnev ./ $target --ignore-time --parallel=10 --exclude-glob .git* --exclude .git/"